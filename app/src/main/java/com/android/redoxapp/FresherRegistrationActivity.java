package com.android.redoxapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.android.redoxapp.helpers.HttpCallResponse;
import com.android.redoxapp.helpers.M;
import com.android.redoxapp.models.Student;
import com.android.redoxapp.models.Sucess;
import com.android.redoxapp.services.FreshStudentRegistrationService;

import java.util.ArrayList;
import java.util.List;

import me.riddhimanadib.formmaster.helper.FormBuildHelper;
import me.riddhimanadib.formmaster.model.FormElement;
import me.riddhimanadib.formmaster.model.FormHeader;
import me.riddhimanadib.formmaster.model.FormObject;
import retrofit2.Response;

public class FresherRegistrationActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private FormBuildHelper mFormBuilder;
    private Button btnFreshSubmit;
    private FormElement feSurname,feGivenName,feFathername,feFatherphone,feFatheraltphone,
            feStudentphone,feStudentaltphone,feDob,feEmail,feGender,feQualification,feReligion,fecaste,feAddharno,feHounseno,fevillage,femandal
            ,festate,fedistrict;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fresher_registration);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mFormBuilder = new FormBuildHelper(this, mRecyclerView);
        btnFreshSubmit=(Button)findViewById(R.id.btnFreshSubmit);

        createForm();

        btnFreshSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(feGivenName.getValue().isEmpty() ||feSurname.getValue().isEmpty() ||feAddharno.getValue().isEmpty()
                        ||feFathername.getValue().isEmpty() ||feFatherphone.getValue().isEmpty() ||feFatheraltphone.getValue().isEmpty()
                        ||feStudentphone.getValue().isEmpty() ||feStudentaltphone.getValue().isEmpty() ||feEmail.getValue().isEmpty()
                        ||feQualification.getValue().isEmpty() ||feReligion.getValue().isEmpty() ||fecaste.getValue().isEmpty()
                        ||feGender.getValue().isEmpty() ||feHounseno.getValue().isEmpty() ||fevillage.getValue().isEmpty()
                        ||femandal.getValue().isEmpty() ||festate.getValue().isEmpty() ||fedistrict.getValue().isEmpty())
                {
                  M.T(FresherRegistrationActivity.this,"Please fill all the feilds");
                }
                else
                {
                    Student mStudent=new Student();
                    mStudent.setFirstname(feGivenName.getValue());
                    mStudent.setSurname(feSurname.getValue());
                    mStudent.setAadhar(feAddharno.getValue());
                    mStudent.setFathername(feFathername.getValue());
                    mStudent.setFmobileno(feFatherphone.getValue());
                    mStudent.setFmobileno2(feFatheraltphone.getValue());
                    mStudent.setSmobile1(feStudentphone.getValue());
                    mStudent.setSmobile2(feStudentaltphone.getValue());
                    mStudent.setEmail(feEmail.getValue());
                    mStudent.setQualification(feQualification.getValue());
                    mStudent.setReligion(feReligion.getValue());
                    mStudent.setCaste(fecaste.getValue());
                    mStudent.setGander(feGender.getValue());
                    mStudent.setHno(feHounseno.getValue());
                    mStudent.setVillage(fevillage.getValue());
                    mStudent.setMandal(femandal.getValue());
                    mStudent.setState(festate.getValue());
                    mStudent.setDistrict(fedistrict.getValue());
                    mStudent.setFresh("true");
                    submitForm(mStudent);
                }
            }
        });
    }

    public void createForm()
    {
        List<String> quals = new ArrayList<>();
        quals.add("SSC");
        quals.add("Intermediate");
        quals.add("B.Sc");
        List<String> religions = new ArrayList<>();
        religions.add("Hindu");
        religions.add("Christian");
        religions.add("Muslim");

        List<String> caste = new ArrayList<>();
        caste.add("ST");
        caste.add("SC");
        caste.add("OC");

        List<String> gender = new ArrayList<>();
        gender.add("Male");
        gender.add("Female");

        FormHeader header = FormHeader.createInstance().setTitle(getResources().getString(R.string.pinfo));
        feSurname = FormElement.createInstance().setType(FormElement.TYPE_EDITTEXT_EMAIL).setTitle(getResources().getString(R.string.surname)).setRequired(true);
        feGivenName = FormElement.createInstance().setType(FormElement.TYPE_EDITTEXT_EMAIL).setTitle(getResources().getString(R.string.GivenName)).setRequired(true);
        feAddharno = FormElement.createInstance().setType(FormElement.TYPE_EDITTEXT_EMAIL).setTitle(getResources().getString(R.string.Aaadhar)).setRequired(true);
        feFathername = FormElement.createInstance().setType(FormElement.TYPE_EDITTEXT_EMAIL).setTitle(getResources().getString(R.string.fathername1)).setRequired(true);
        feFatherphone = FormElement.createInstance().setType(FormElement.TYPE_EDITTEXT_PHONE).setTitle(getResources().getString(R.string.fathermobileno1)).setRequired(true);
        feFatheraltphone = FormElement.createInstance().setType(FormElement.TYPE_EDITTEXT_PHONE).setTitle(getResources().getString(R.string.fathermobileno2));
        feStudentphone = FormElement.createInstance().setType(FormElement.TYPE_EDITTEXT_PHONE).setTitle(getResources().getString(R.string.studentmobilenumber1)).setRequired(true);
        feStudentaltphone = FormElement.createInstance().setType(FormElement.TYPE_EDITTEXT_PHONE).setTitle(getResources().getString(R.string.studentmobilenumber2));
        feDob = FormElement.createInstance().setType(FormElement.TYPE_PICKER_DATE).setTitle(getResources().getString(R.string.DOB)).setRequired(true);

        feEmail = FormElement.createInstance().setType(FormElement.TYPE_EDITTEXT_EMAIL).setTitle(getResources().getString(R.string.Email)).setRequired(true);
        feGender = FormElement.createInstance().setType(FormElement.TYPE_SPINNER_DROPDOWN).setTitle(getResources().getString(R.string.gender)).setOptions(gender).setRequired(true);
        feQualification = FormElement.createInstance().setType(FormElement.TYPE_SPINNER_DROPDOWN).setTitle(getResources().getString(R.string.Qualification)).setOptions(quals).setRequired(true);
        feReligion = FormElement.createInstance().setType(FormElement.TYPE_SPINNER_DROPDOWN).setTitle(getResources().getString(R.string.Religion)).setOptions(religions).setRequired(true);
        fecaste = FormElement.createInstance().setType(FormElement.TYPE_SPINNER_DROPDOWN).setTitle(getResources().getString(R.string.caste)).setOptions(caste).setRequired(true);
        FormHeader Aheader = FormHeader.createInstance().setTitle(getResources().getString(R.string.Address));

        feHounseno = FormElement.createInstance().setType(FormElement.TYPE_EDITTEXT_EMAIL).setTitle(getResources().getString(R.string.H_No)).setRequired(true);
        fevillage = FormElement.createInstance().setType(FormElement.TYPE_EDITTEXT_EMAIL).setTitle(getResources().getString(R.string.Village)).setRequired(true);
        femandal = FormElement.createInstance().setType(FormElement.TYPE_EDITTEXT_EMAIL).setTitle(getResources().getString(R.string.Manadal)).setRequired(true);
        festate = FormElement.createInstance().setType(FormElement.TYPE_EDITTEXT_EMAIL).setTitle(getResources().getString(R.string.State)).setRequired(true);
        fedistrict = FormElement.createInstance().setType(FormElement.TYPE_EDITTEXT_EMAIL).setTitle(getResources().getString(R.string.Dist)).setRequired(true);


        // add them in a list
        List<FormObject> formItems = new ArrayList<>();
        formItems.add(header);
        formItems.add(feSurname);
        formItems.add(feGivenName);
        formItems.add(feAddharno);
        formItems.add(feFathername);
        formItems.add(feFatherphone);
        formItems.add(feFatheraltphone);
        formItems.add(feDob);
        formItems.add(feStudentphone);
        formItems.add(feStudentaltphone);
        formItems.add(feEmail);
        formItems.add(feGender);
        formItems.add(feQualification);
        formItems.add(feReligion);
        formItems.add(fecaste);
        formItems.add(Aheader);
        formItems.add(feHounseno);
        formItems.add(fevillage);
        formItems.add(femandal);
        formItems.add(festate);
        formItems.add(fedistrict);
        // build and display the form
        mFormBuilder.addFormElements(formItems);
        mFormBuilder.refreshView();
    }
    public void submitForm(Student mStudent)
    {
        M.showLoadingDialog(FresherRegistrationActivity.this);
        FreshStudentRegistrationService.submitFreshRegistration(FresherRegistrationActivity.this, mStudent, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<Sucess> mRes = ( Response<Sucess>)obj;
                Sucess mSucess = mRes.body();
                if(mSucess.getStatus().equalsIgnoreCase("success"))
                {
                    M.T(FresherRegistrationActivity.this,getResources().getString(R.string.successful));
                    startActivity(new Intent(FresherRegistrationActivity.this,MenuActivtiy.class));
                }
                else
                {
                    M.T(FresherRegistrationActivity.this,getResources().getString(R.string.tryagain));
                    startActivity(new Intent(FresherRegistrationActivity.this,MenuActivtiy.class));
                }
                M.hideLoadingDialog();
            }

            @Override
            public void OnFailure(int ErrorCode) {
                M.T(FresherRegistrationActivity.this,getResources().getString(R.string.tryagain));
                startActivity(new Intent(FresherRegistrationActivity.this,MenuActivtiy.class));

                M.hideLoadingDialog();
            }
        });
    }
}
