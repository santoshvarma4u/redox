package com.android.redoxapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MenuActivtiy extends AppCompatActivity implements View.OnClickListener {

    protected Button btnregistration;
    protected Button btncompregistration;
    protected Button btntracking;
    protected Button whatrpl;
    protected Button btnrplsurvey;
    protected Button btnEducationNews;
    protected Button btnCurrentAffairs;
    protected Button btnGeneralKnowledge;
    protected Button btnCareerTips;
    protected Button btnTools;
    protected Button btnFeedback;
    protected Button btnAboutus;
    protected Button btnContactus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_menu_activtiy);
        initView();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnregistration) {
            startActivity(new Intent(MenuActivtiy.this, SelectType.class));
        } else if (view.getId() == R.id.btncompregistration) {
            Toast.makeText(MenuActivtiy.this, "Coming soon", Toast.LENGTH_SHORT).show();
        } else if (view.getId() == R.id.btntracking) {
            Toast.makeText(MenuActivtiy.this, "Coming soon", Toast.LENGTH_SHORT).show();
        } else if (view.getId() == R.id.whatrpl) {
            startActivity(new Intent(MenuActivtiy.this,RPLContentActivity.class));
        } else if (view.getId() == R.id.btnrplsurvey) {
            startActivity(new Intent(MenuActivtiy.this, RPLSurveyActivity.class));
        } else if (view.getId() == R.id.btnEducationNews) {

        } else if (view.getId() == R.id.btnCurrentAffairs) {

        } else if (view.getId() == R.id.btnGeneralKnowledge) {

        } else if (view.getId() == R.id.btnCareerTips) {

        } else if (view.getId() == R.id.btnTools) {
            startActivity(new Intent(MenuActivtiy.this,WebToolsActivity.class));

        } else if (view.getId() == R.id.btnFeedback) {
            startActivity(new Intent(MenuActivtiy.this,FeedbackActivity.class));
        } else if (view.getId() == R.id.btnAboutus) {

        } else if (view.getId() == R.id.btnContactus) {

        }
    }

    private void initView() {
        btnregistration = (Button) findViewById(R.id.btnregistration);
        btnregistration.setOnClickListener(MenuActivtiy.this);
        btncompregistration = (Button) findViewById(R.id.btncompregistration);
        btncompregistration.setOnClickListener(MenuActivtiy.this);
        btnrplsurvey = (Button) findViewById(R.id.btnrplsurvey);
        btnrplsurvey.setOnClickListener(MenuActivtiy.this);
        btntracking = (Button) findViewById(R.id.btntracking);
        btntracking.setOnClickListener(MenuActivtiy.this);
        whatrpl = (Button) findViewById(R.id.whatrpl);
        whatrpl.setOnClickListener(MenuActivtiy.this);
        btnEducationNews = (Button) findViewById(R.id.btnEducationNews);
        btnEducationNews.setOnClickListener(MenuActivtiy.this);
        btnCurrentAffairs = (Button) findViewById(R.id.btnCurrentAffairs);
        btnCurrentAffairs.setOnClickListener(MenuActivtiy.this);
        btnGeneralKnowledge = (Button) findViewById(R.id.btnGeneralKnowledge);
        btnGeneralKnowledge.setOnClickListener(MenuActivtiy.this);
        btnCareerTips = (Button) findViewById(R.id.btnCareerTips);
        btnCareerTips.setOnClickListener(MenuActivtiy.this);
        btnTools = (Button) findViewById(R.id.btnTools);
        btnTools.setOnClickListener(MenuActivtiy.this);
        btnFeedback = (Button) findViewById(R.id.btnFeedback);
        btnFeedback.setOnClickListener(MenuActivtiy.this);
        btnAboutus = (Button) findViewById(R.id.btnAboutus);
        btnAboutus.setOnClickListener(MenuActivtiy.this);
        btnContactus = (Button) findViewById(R.id.btnContactus);
        btnContactus.setOnClickListener(MenuActivtiy.this);
    }
}
