package com.android.redoxapp.services;

import android.content.Context;

import com.android.redoxapp.R;
import com.android.redoxapp.helpers.HttpCallResponse;
import com.android.redoxapp.models.RplSurvey;
import com.android.redoxapp.models.Student;
import com.android.redoxapp.models.Sucess;

import me.riddhimanadib.formmaster.model.FormElement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public class FreshStudentRegistrationService extends APIService {

   public static void submitFreshRegistration(final Context context, final Student mStudent, final HttpCallResponse httpCallResponse)
   {
       StudentRegistrationInterface mStudentRegistrationInterface=retrofit.create(StudentRegistrationInterface.class);
       Call<Sucess> mSucessCall=mStudentRegistrationInterface.submitRegistration(mStudent.getFresh(),mStudent.getSurname(),mStudent.getFirstname(),mStudent.getFathername(),mStudent.getAadhar(),mStudent.getFmobileno(),mStudent.getFmobileno2(),mStudent.getSmobile1(),mStudent.getSmobile2(),mStudent.getEmail(),mStudent.getQualification(),mStudent.getReligion(),mStudent.getCaste(),mStudent.getGander(),mStudent.getAddress(),mStudent.getHno(),mStudent.getVillage(),mStudent.getMandal(),mStudent.getState(),mStudent.getDistrict(),mStudent.getDob(),mStudent.getPhoto());
       mSucessCall.enqueue(new Callback<Sucess>() {
           @Override
           public void onResponse(Call<Sucess> call, Response<Sucess> response) {
               if(response.isSuccessful())
               {
                   httpCallResponse.OnSuccess(response);
               }
               else
               {
                   httpCallResponse.OnFailure(response.code());
               }
           }

           @Override
           public void onFailure(Call<Sucess> call, Throwable t) {

           }
       });
   }

  public static void submitFeedback(final Context context, final String name,final String desc, final HttpCallResponse httpCallResponse)
   {
       FeedbackInterface mStudentRegistrationInterface=retrofit.create(FeedbackInterface.class);
       Call<Sucess> mSucessCall=mStudentRegistrationInterface.submitfeedback(name,desc);
       mSucessCall.enqueue(new Callback<Sucess>() {
           @Override
           public void onResponse(Call<Sucess> call, Response<Sucess> response) {
               if(response.isSuccessful())
               {
                   httpCallResponse.OnSuccess(response);
               }
               else
               {
                   httpCallResponse.OnFailure(response.code());
               }
           }

           @Override
           public void onFailure(Call<Sucess> call, Throwable t) {

           }
       });
   }

   public static void submitRplSurvey(final Context context, final RplSurvey mRplSurvey, final HttpCallResponse httpCallResponse)
   {
       RplRegistrationInterface mRplRegistrationInterface=retrofit.create(RplRegistrationInterface.class);
       Call<Sucess> mSucessCall=mRplRegistrationInterface.submitSurvey(mRplSurvey.getName(),mRplSurvey.getFathername(),mRplSurvey.getDob(),mRplSurvey.getMobile(),mRplSurvey.getCaste(),mRplSurvey.getAadhar(),mRplSurvey.getEmail(),mRplSurvey.getCompanyname(),mRplSurvey.getCompanytype(),mRplSurvey.getLocationunit(),mRplSurvey.getContractorname(),mRplSurvey.getContractorph(),mRplSurvey.getDesignation(),mRplSurvey.getQualification(),mRplSurvey.getExperience(),mRplSurvey.getPsc(),mRplSurvey.getStce(),mRplSurvey.getSsc(),mRplSurvey.getLssdc(),mRplSurvey.getAccident(),mRplSurvey.getSupervisor(),mRplSurvey.getTraining(),mRplSurvey.getPharmacy());
       mSucessCall.enqueue(new Callback<Sucess>() {
           @Override
           public void onResponse(Call<Sucess> call, Response<Sucess> response) {
               if(response.isSuccessful())
               {
                   httpCallResponse.OnSuccess(response);
               }
               else
               {
                   httpCallResponse.OnFailure(response.code());
               }
           }

           @Override
           public void onFailure(Call<Sucess> call, Throwable t) {

           }
       });
   }

    public interface StudentRegistrationInterface {


        /* public String fresh,surname,firstname,fathername,aadhar,fmobileno,fmobileno2,
        smobile1,smobile2,email,qualification,religion,caste,gander,address,hno,village,mandal,state,
        district,dob,photo,photo,expmonths,curworkingcomp,hrcontractorname,hrcontractorno,bankno,skillscert,certhelp,upgradeskills;*/
        @FormUrlEncoded
        @POST("freshregistration.php")
        Call<Sucess> submitRegistration(@Field("fresh") String fresh,@Field("surname") String surname,@Field("firstname") String firstname,
                                        @Field("fathername") String fathername,@Field("aadhar") String aadhar,@Field("fmobileno") String fmobileno,
                                        @Field("fmobileno2") String fmobileno2,@Field("smobile1") String smobile1,@Field("smobile2") String smobile2,
                                        @Field("email") String email,@Field("qualification") String qualification,@Field("religion") String religion,
                                        @Field("caste") String caste,@Field("gander") String gander,@Field("address") String address,
                                        @Field("hno") String hno,@Field("village") String village,@Field("mandal") String mandal,
                                        @Field("state") String state,@Field("district") String district,@Field("dob") String dob,
                                        @Field("photo") String photo);
    }

    public interface RplRegistrationInterface {


        /* name,fathername,dob,mobile,caste,aadhar,email,companyname,companytype,locationunit,contractorname,contractorph,designation,qualification,
          experience,psc,stce,ssc,lssdc,accident,supervisor,training,pharmacy;*/

        @FormUrlEncoded
        @POST("rplreg.php")
        Call<Sucess> submitSurvey(@Field("name") String name,@Field("fathername") String fathername,@Field("dob") String dob,@Field("mobile") String mobile,
                                  @Field("caste") String caste,@Field("aadhar") String aadhar,@Field("email") String email,@Field("companyname") String companyname,
                                  @Field("companytype") String companytype,@Field("locationunit") String locationunit,@Field("contractorname") String contractorname,
                                  @Field("contractorph") String contractorph,@Field("designation") String designation,@Field("qualification") String qualification,
                                  @Field("experience") String experience,@Field("psc") String psc,
                                  @Field("stce") String stce,@Field("ssc") String ssc,@Field("lssdc") String lssdc,@Field("accident") String accident,
                                  @Field("supervisor") String supervisor,@Field("training") String training,@Field("pharmacy") String pharmacy);
    }

    public interface FeedbackInterface {


        /* name,fathername,dob,mobile,caste,aadhar,email,companyname,companytype,locationunit,contractorname,contractorph,designation,qualification,
          experience,psc,stce,ssc,lssdc,accident,supervisor,training,pharmacy;*/

        @FormUrlEncoded
        @POST("feedback.php")
        Call<Sucess> submitfeedback(@Field("name") String name,@Field("desc") String description);
    }
}
